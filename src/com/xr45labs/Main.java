package com.xr45labs;

import java.util.List;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Libro l1 = new Libro("lb1", "Libro 1", "autor1");
        Libro l2 = new Libro("lb2", "Libro 2", "autor2");
        Libro l3 = new Libro("lb3", "Libro 3", "autor3");



        Lista lista = new Lista();

        lista.insertarPrincipio(l1);
        lista.insertarPrincipio(l3);

        System.out.print(lista.contar());

    }
}
